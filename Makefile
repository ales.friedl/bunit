.PHONY: default all build build_test build_third_party install test unittest log_dir clean clean_build clean_install

PROJECT = bunit-runner
LIBBUNIT = bunit
BUNIT_RUN_SUITE = bunit-run-suite
BUNIT_RUN_TEST = bunit-run-test
LOG_DIR = build/log
THIRD_PARTY = bash-colors

default: all

all: build build_test

build: build/bin/${PROJECT} build/lib/${LIBBUNIT} build/libexec/${BUNIT_RUN_SUITE} build/libexec/${BUNIT_RUN_TEST} build_third_party

build/bin/${PROJECT}: src/${PROJECT}.sh
	@mkdir -p ${@D}
	@cat $< >$@
	@chmod a+x $@

build/lib/%: src/lib/%.sh
	@mkdir -p ${@D}
	@cp $< $@

build/libexec/%: src/libexec/%.sh
	@mkdir -p ${@D}
	@cat $< >$@
	@chmod a+x $@

build_test: build build_third_party
	@cp -r test build
	@find build/test -type f -name '*.sh' -exec chmod \+x {} +

build_third_party:
	@mkdir -p "build"
	@cp -r third_party build
	@cd build/third_party/${THIRD_PARTY} && make build

install: bin/${PROJECT} bin/lib/${LIBBUNIT} bin/libexec/${BUNIT_RUN_SUITE} bin/libexec/${BUNIT_RUN_TEST}

bin/%: build/bin/%
	@mkdir -p ${@D}
	@cp $< $@

bin/lib/%: build/lib/%
	@mkdir -p ${@D}
	@cp $< $@

bin/libexec/%: build/libexec/%
	@mkdir -p ${@D}
	@cp $< $@

unittest: build_test log_dir
	@BUNIT_COLORS=false build/test/run.sh

test: unittest

log_dir:
	@mkdir -p ${LOG_DIR}

clean: clean_build clean_install

clean_build:
	@rm -fr build

clean_install:
	@rm -fr bin

