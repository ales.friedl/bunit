#!/bin/bash

BUNIT_RUNNER_SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# specify where logs should be created
BUNIT_LOG_DIR="${BUNIT_LOG_DIR:-$BUNIT_RUNNER_SCRIPT_DIR/log}"
# specify colors
BUNIT_COLORS=${BUNIT_COLORS:-false}

# shellcheck disable=SC1090 # best-effort
. "$BUNIT_RUNNER_SCRIPT_DIR/../lib/bunit"

# specify where tested project is located
PROJECT_ROOT="${PROJECT_ROOT:-$BUNIT_RUNNER_SCRIPT_DIR}"
# specify where tests are located
BUNIT_TEST_DIR="${BUNIT_TEST_DIR:-$BUNIT_RUNNER_SCRIPT_DIR}"
export BUNIT_TEST_DIR
assert cd "$BUNIT_TEST_DIR" || exit 1

shopt -s failglob
test_suite_failed=0
for test_suite_dir in *; do
	[[ ! -d "$test_suite_dir" ]] && continue
	if ! "$BUNIT_RUN_SUITE" "$test_suite_dir"; then
		test_suite_failed=1
	fi
done
assert_equal "$test_suite_failed" "0"
