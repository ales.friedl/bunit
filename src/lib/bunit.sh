#!/bin/bash

BUNIT_SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
BUNIT_SCRIPT_FULLNAME="${BUNIT_SCRIPT_DIR}${BUNIT_SCRIPT_DIR:+/}${0##*/}"

# specify where logs should be created
BUNIT_LOG_DIR="${BUNIT_LOG_DIR:-$BUNIT_SCRIPT_DIR/log}"
# specify colors
BUNIT_COLORS=${BUNIT_COLORS:-false}

BUNIT_LOG_FILE="$BUNIT_LOG_DIR/test.log"
BUNIT_SETUP_LOG_FILE="$BUNIT_LOG_FILE" # "$BUNIT_LOG_DIR/test-setup.log"
BUNIT_LOG_CONTEXT="" # test or setup

BUNIT_LOG_ID="0"

WORKSPACE="${BUNIT_SCRIPT_DIR}${BUNIT_SCRIPT_DIR:+/}.."
# shellcheck disable=SC1090
. "$WORKSPACE/third_party/bash-colors/build/lib/colors"

BUNIT_RUN_SUITE="${BUNIT_SCRIPT_DIR}/../libexec/bunit-run-suite"
BUNIT_RUN_TEST="${BUNIT_SCRIPT_DIR}/../libexec/bunit-run-test"

__output_formatter() {
	local -r tag="$1"
	local -r log="$2"
	declare -A context_mark
	context_mark["setup"]=" "
	context_mark["test"]=" | "
	while IFS="" read -r line; do
		printf "%s: %s: %s: [%s]%s%s\\n" "$(date --rfc-3339=seconds)" \
			"($tag)" "${BUNIT_SCRIPT_FULLNAME##*/}" "$BUNIT_LOG_ID" "${context_mark[$BUNIT_LOG_CONTEXT]}" "$line" >> "$log"
			#"($tag)" "$BUNIT_SCRIPT_FULLNAME" "$BUNIT_LOG_ID" "${context_mark[$BUNIT_LOG_CONTEXT]}" "$line" >> "$log"
	done
}

__stderr_formatter() {
	__output_formatter "EE" "$@"
}

__stdout_formatter() {
	__output_formatter "II" "$@"
}

assert() {
	"$@" >/dev/null || return 1
}


__context_is() {
	local -r expected_context="$1"
	if [[ "$BUNIT_LOG_CONTEXT" != "test" ]]; then
		printf "Context is not \"%s\"\\n" "$expected_context" >&2
		return 1
	fi
}

assert_success() {
	__context_is "test" || return 1
	"$@" >/dev/null || return 1
}

assert_failure() {
	__context_is "test" || return 1
	# shellcheck disable=SC2069
	"$@" 2>&1 1>/dev/null || return 0
}

__is_logfile_writable() {
	local -r log="$1"
	touch "$log"
	[[ -w "$log" ]]
}

__exit_logfile_not_writable() {
	local -r log="$1"
	printf "logfile not available (%s)\\n" "$log" >&2
	printf "pwd: %s\\n" "$(pwd)" >&2
	exit 1
}

list_arr() {
	local -n a=$1
	local -r a_name="$1"
	mapfile -d '' sorted_keys < <(printf '%s\0' "${!a[@]}" | sort -z)
	for key in "${sorted_keys[@]}"; do
		printf "%s[\"%s\"]=\"%s\"\\n" "$a_name" "$key" "${a[$key]}"
	done
}

BUNIT_STATS_TESTS_TIME=0
__measure_time() {
	local start_time
	local stop_time
	start_time="$(date "+%s")"
	"$@"
	stop_time="$(date "+%s")"
	BUNIT_STATS_TESTS_TIME="$((stop_time - start_time))"
}

BUNIT_STATS_TESTS_FAILED=0
BUNIT_STATS_TESTS_PASSED=0
BUNIT_STATS_TESTS_RUN=0
bunit_run_test_suite_test_cases() {
	local -n a=$1
	local -r test_suite_dir="$2"
	local stats_suite_tests_passed=0
	local stats_suite_tests_failed=0
	local stats_suite_tests_run=0
	mapfile -d '' sorted_keys < <(printf '%s\0' "${!a[@]}" | sort -z)
	for key in "${sorted_keys[@]}"; do
		((BUNIT_LOG_ID++))
		local test_suite_test_file_and_test="${a[$key]}"
		local test_suite_test_file=${test_suite_test_file_and_test%%:*}
		local test_suite_test_file_test=${test_suite_test_file_and_test##*:}
		printf "${cinfo}Running suite \"%s\" (file \"%s\") test case #%d/%d \"%s\"\\n${cnone}" "$test_suite_dir" "$test_suite_test_file" "$((key+1))" "${#sorted_keys[@]}" "$test_suite_test_file_test"

		BUNIT_RUN_TEST_TEST_DIR="${BUNIT_RUN_TEST_TEST_DIR:-$BUNIT_TEST_DIR}"
		export BUNIT_RUN_TEST_TEST_DIR

		if "$BUNIT_RUN_TEST" "$test_suite_dir/$test_suite_test_file" "$test_suite_test_file_test"; then
			printf "%b\\n" "${cok}PASSED${cnone}"
			((stats_suite_tests_passed++))
			((BUNIT_STATS_TESTS_PASSED++))
		else
			printf "%b\\n" "${cko}FAILED${cnone}"
			((stats_suite_tests_failed++))
			((BUNIT_STATS_TESTS_FAILED++))
		fi
		((stats_suite_tests_run++))
		((BUNIT_STATS_TESTS_RUN++))
	done
	[[ ${#sorted_keys[@]} -eq $((stats_suite_tests_passed + stats_suite_tests_failed)) ]]
}

bunit_print_stats() {
	printf "RESULTS: %s tests passed.  %s tests failed.  %s tests total.  Run time: %s seconds.\\n" \
		"$BUNIT_STATS_TESTS_PASSED" "$BUNIT_STATS_TESTS_FAILED" "$BUNIT_STATS_TESTS_RUN" "$BUNIT_STATS_TESTS_TIME"
}

setup() {
	__is_logfile_writable "$BUNIT_SETUP_LOG_FILE" || __exit_logfile_not_writable "$BUNIT_SETUP_LOG_FILE"
	BUNIT_LOG_CONTEXT="setup"
	exec 1>> >(__stdout_formatter "$BUNIT_SETUP_LOG_FILE")
	exec 2>> >(__stderr_formatter "$BUNIT_SETUP_LOG_FILE")
}

setup_end() {
	__is_logfile_writable "$BUNIT_LOG_FILE" || __exit_logfile_not_writable "$BUNIT_LOG_FILE"
	BUNIT_LOG_CONTEXT="test"
	exec 1>> >(__stdout_formatter "$BUNIT_LOG_FILE")
	exec 2>> >(__stderr_formatter "$BUNIT_LOG_FILE")
}

set_colors() {
	local colors=$1

	setup_colors "$colors"

	#cok="$lightgreen"
	#cko="$lightred"
}

assert_equal() {
	[[ "$1" == "$2" ]] || {
		printf "${cko}assert_equal failed: \"%s\" != \"%s\"${cnone}\\n" "$1" "$2"
		return 1
	}
}

assert_not_equal() {
	[[ "$1" != "$2" ]] || {
		printf "${cko}assert_not_equal failed: \"%s\" != \"%s\"${cnone}\\n" "$1" "$2"
		return 1
	}
}

assert_all_tests_passed() {
	assert_equal "$BUNIT_STATS_TESTS_FAILED" "0"
}

setup

set_colors "$BUNIT_COLORS"

printf "Hallo \"%s\"\\n" "$BUNIT_SCRIPT_FULLNAME"
