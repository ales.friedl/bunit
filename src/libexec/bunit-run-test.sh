#!/bin/bash

BUNIT_RUN_TEST_SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# specify where logs should be created
BUNIT_LOG_DIR="${BUNIT_LOG_DIR:-$BUNIT_RUNNER_SCRIPT_DIR/log}"
# specify colors
BUNIT_COLORS=${BUNIT_COLORS:-false}

# shellcheck disable=SC1090 # best-effort
. "$BUNIT_RUN_TEST_SCRIPT_DIR/../lib/bunit"

# specify where tested project is located
PROJECT_ROOT="${PROJECT_ROOT:-$BUNIT_RUNNER_SCRIPT_DIR}"
# specify where tests are located
BUNIT_RUN_TEST_TEST_DIR="${BUNIT_RUN_TEST_TEST_DIR:-$BUNIT_RUN_TEST_SCRIPT_DIR}"
assert cd "$BUNIT_RUN_TEST_TEST_DIR"

TEST_FILE="$1"
TEST="$2"

# shellcheck disable=SC1090
. "$BUNIT_RUN_TEST_TEST_DIR/$TEST_FILE"

if [ ! "$(type -t "$TEST" || true)" = "function" ]; then
	exit 1
fi

setup_end
"$TEST"
