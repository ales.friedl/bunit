#!/bin/bash

BUNIT_RUN_SUITE_SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# specify where logs should be created
BUNIT_LOG_DIR="${BUNIT_LOG_DIR:-$BUNIT_RUN_SUITE_SCRIPT_DIR/log}"
# specify colors
BUNIT_COLORS=${BUNIT_COLORS:-false}

# shellcheck disable=SC1090 # best-effort
. "$BUNIT_RUN_SUITE_SCRIPT_DIR/../lib/bunit"

# specify where tested project is located
PROJECT_ROOT="${PROJECT_ROOT:-$BUNIT_RUN_SUITE_SCRIPT_DIR}"

BUNIT_RUN_SUITE_SUITE_DIR="$1"
BUNIT_RUN_SUITE_SUITE_DIR="$BUNIT_TEST_DIR/${BUNIT_RUN_SUITE_SUITE_DIR##*/}"
assert cd "$BUNIT_RUN_SUITE_SUITE_DIR" || exit 1

declare -A test_suite_test_cases
declare -a test_suite_test_cases_arr

for test_suite_test_file in "$BUNIT_RUN_SUITE_SUITE_DIR"/test_*.sh; do
	test_suite_test_cases["${BUNIT_RUN_SUITE_SUITE_DIR##*/}"]+="$(sed -n -e "s/^\\(function \\)\\?\\(test[^(]*\\)([^)]*).*$/${test_suite_test_file##*/}:\\2/p" "$test_suite_test_file") "
	printf "Test suite \"%s\" add file \"%s\"\\n" "${BUNIT_RUN_SUITE_SUITE_DIR##*/}" "$test_suite_test_file"
	# shellcheck disable=SC1090
	assert . "$test_suite_test_file" || exit 1
done

# shellcheck disable=SC2034,SC2206
test_suite_test_cases_arr=(${test_suite_test_cases["${BUNIT_RUN_SUITE_SUITE_DIR##*/}"]})
bunit_run_test_suite_test_cases test_suite_test_cases_arr "${BUNIT_RUN_SUITE_SUITE_DIR##*/}"

bunit_print_stats

assert_all_tests_passed
