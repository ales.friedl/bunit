# Changelog

## Unreleased

## 1.1.0 (2020/12/20)

- setup context usage fixed
- setup context usage simplified
- result code added
- changelog added

## 1.0.0 (2018/08/30)

- unit tests added
- GitLab CI added

## 0.0.1 (2018/08/30)

- support for multiple test suites with multiple tests
- setup and test context to differentiate between setup and test phase
